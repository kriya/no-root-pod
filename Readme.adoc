= No Privileged Pod

//tag::abstract[]

A Pod by default has lease restriction which
allows for privileged actions.  Processes within the container get almost the
same privileges that are available to processes outside a container

//end::abstract[]

Default Pod configuration has a large attack surface that
can result in full host takeover.  
Kubernetes Pod Security Policy  is a
cluster-level resource that controls security sensitive aspects of the pod
specification. The PodSecurityPolicy objects define a set of conditions that a
pod must run with in order to be accepted into the system.

//tag::lab[]

== Lab

=== Task 0

*Fork* and clone this repository.
Install `kubernetes`, and `make` on your system. 
Enable both `Pod Security` and `Network` policies.

. Build the program: `make build`.
. Run it: `make run`.

=== Task 1

Review `deployment.yaml` and `policy.yaml` files. 
Why it is allowed to created privileged pod?
Find out the security weakness in the policy.

=== Task 2

By default pod can have execute privileged actions.
Edit `policy.yaml` and restrict both privileged and privilege escalation 
specifications. Forbid containers to run with root privileges.

=== Task 3

Run `make clean`, `make build`, and `make run`
If you have applied policies correctly, 
the last command should fail with a permission denied warning

=== Task 4 
 
Check out the `patch` branch and review the changes. 


//end:lab[]

//tag::references[]

== References

* https://kubernetes.io/docs/concepts/policy/pod-security-policy/

//end::references[]
